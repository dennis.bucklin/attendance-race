/* FIX THE LOAD THE SCHEDULE THING ON LATER DAYS */

function globalAddEventListener(name, className, callback) {
  document.addEventListener(name, e => {
    if (e.target.classList.contains(className)) {
      callback(e);
    }
  })
}

window.addEventListener('DOMContentLoaded', () => {
  const tourButton = document.getElementById('tour-button');
  const hasIntro = document.querySelector('[data-intro]');
  if (hasIntro && tourButton && introJs) {
    tourButton.classList.remove('is-hidden');
    tourButton.addEventListener('click', () => {
      introJs().setOptions({showStepNumbers: true}).start();
    })
  }

  const sessionExpiry = document.getElementById('session-expiry');
  if (sessionExpiry) {
    const when = new Date(sessionExpiry.dateTime);
    const span = when.valueOf() - new Date().valueOf();
    const alertSpan = span - (60 * 10 * 1000);  // Ten-minute warning...
    setTimeout(() => {
      const warningModal = document.getElementById('session-expiry-warning');
      if (warningModal) {
        warningModal.classList.add('is-active');
        const closer = warningModal.querySelector('.modal-close');
        closer.addEventListener('click', () => warningModal.classList.remove('is-active'));
        const refresher = warningModal.querySelector('#session-expiry-refresh');
        refresher.addEventListener('click', () => window.location.reload());

        // < 1% chance to see the T-Rex!
        if (Math.random() < 0.01) {
          const images = warningModal.querySelectorAll('.t-rex');
          for (const image of images) {
            image.classList.toggle('is-hidden');
          }
        }
      }
    }, alertSpan);
  }

  const notifications = document.querySelectorAll('.notification .delete');
  notifications.forEach(button => {
    const notification = button.parentNode;
    button.addEventListener('click', () => {
      notification.parentNode.removeChild(notification);
    });
    notification.addEventListener('animationend', e => {
      e.target.parentNode.removeChild(e.target);
    });
  });

  const burgers = document.querySelectorAll('.navbar-burger');
  burgers.forEach(burger => {
    burger.addEventListener('click', () => {
      const target = document.getElementById(burger.dataset.target);
      burger.classList.toggle('is-active');
      target.classList.toggle('is-active');
    });
  });

  const tabGroups = document.querySelectorAll('.tabs.interactive');
  for (const tabGroup of tabGroups) {
    const tabs = tabGroup.querySelectorAll('li');
    for (const tab of tabs) {
      function clearTabs () {
        for (const tab of tabs) {
          tab.classList.remove('is-active');
          const target = document.getElementById(tab.dataset.target);
          if (target) {
            target.classList.add('is-hidden');
          }
        }
      }
      tab.addEventListener('click', () => {
        clearTabs();
        tab.classList.add('is-active');
        const target = document.getElementById(tab.dataset.target);
        if (target) {
          target.classList.remove('is-hidden');
        }
      });
    }
  }

  const revealables = document.querySelectorAll('.can-reveal');
  for (const el of revealables) {
    el.addEventListener('click', () => {
      el.innerHTML = el.dataset.content;
    });
  }

  const immediateModals = document.querySelectorAll('.immediate-modal');
  if (immediateModals.length > 0) {
    immediateModals[0].classList.add('is-active');
  }

  function createNotification (bulmaClass, message) {
    const notification = document.createElement('div');
    notification.className = `from-right notification ${bulmaClass}`;
    notification.innerText = message;

    const errorButton = document.createElement('button');
    errorButton.className = 'delete';
    errorButton.addEventListener('click', () => {
      errorButton.parentElement.remove();
    });

    notification.appendChild(errorButton);
    document.querySelector('#notification-holder').appendChild(notification);
    notification.addEventListener('animationend', e => {
      e.target.parentNode.removeChild(e.target);
    });
  }

  function showErrorMessage (message) {
    createNotification('is-danger', message);
  }

  function showSuccessMessage (message) {
    createNotification('is-success', message);
  }

  function setElementToValueIfEmpty (id, dbVal) {
    const field = document.getElementById(id);
    if (field.value.trim().length === 0) {
      field.value = dbVal;
    }
  }

  function dateFromDateTime(date) {
    if (!date) {
      return date;
    }
    date = new Date(date);
    const year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    if (month < 10) {
      month = "0" + month;
    }
    if (day < 10) {
      day = "0" + day;
    }
    return `${year}-${month}-${day}`;
  }

  const fieldId = document.querySelector('.is-learn #id_learn_id');
  if (fieldId) {
    fieldId.addEventListener('focusout', async (e) => {
      const inputId = e.target.value.trim();
      const submitButton = document.getElementById('jeffs_enthusiastic_submit_button');
      if (inputId) {
        submitButton.classList.add('is-loading');
        submitButton.classList.add('disabled');
        submitButton.setAttribute('disabled', true);
        const response = await fetch(`/cohorts/${inputId}/learn-details/`);
        submitButton.classList.remove('is-loading');
        let cohortInfo;
        if (response.ok) {
          try {
            cohortInfo = await response.json();
          } catch (err) {
            cohortInfo = null;
            console.log('Response Error fetching API data:', err);
          }
          if (cohortInfo) {
            setElementToValueIfEmpty('id_name', cohortInfo.cohort_name);
            setElementToValueIfEmpty('id_cohort_code', cohortInfo.cohort_code);
            setElementToValueIfEmpty('id_start_date', dateFromDateTime((cohortInfo.cohort_starts_on || "").replace("Z", "")));
            setElementToValueIfEmpty('id_end_date', dateFromDateTime((cohortInfo.cohort_ends_on || "").replace("Z", "")));
            setElementToValueIfEmpty('id_lms_url', cohortInfo.cohort_lms_url);
            showSuccessMessage('Learn information synchronized to empty fields');
          }
        } else if (response.status === 404) {
          const data = await response.json();
          showErrorMessage(data.message);
        } else {
          showErrorMessage('We encountered an error. Tech support will address it soon.');
        }
        submitButton.removeAttribute('disabled');
        submitButton.classList.remove('disabled');
      }
    });
  }

  const cohort_selector = document.getElementById('attendance_cohort_id');
  if (cohort_selector) {
    cohort_selector.addEventListener('change', e => {
      const template = cohort_selector.form.dataset.actionTemplate;
      const value = cohort_selector.options[cohort_selector.selectedIndex].value;
      cohort_selector.form.action = template.replace(/\/0\//, `/${value}/`);
    });
  }

  function updateSchedule() {
    const currentSchedule = document.querySelector('#schedule .schedule-table');
    const times = Array.from(currentSchedule.querySelectorAll('tbody tr td time'));
    const now = new Date();
    for (let [index, time] of Object.entries(times)) {
      index = parseInt(index);
      time.parentNode.parentNode.classList.remove('is-active');
      time.parentNode.parentNode.classList.remove('is-inactive');
      const when = new Date(time.getAttribute('datetime'));
      let next = null;
      if (index + 1 < times.length) {
        next = new Date(times[index + 1].getAttribute('datetime'));
      }
      if (next && now > next) {
        time.parentNode.parentNode.classList.add('is-inactive');
      } else if (when < now && (next === null || next > now)) {
        time.parentNode.parentNode.classList.add('is-active');
      }
    }
  }

  async function loadSchedule(cohortId, dayId) {
    let loadingDiv = document.getElementById('loading-div-for-schedule');
    if (!loadingDiv) {
      loadingDiv = document.createElement('div');
      loadingDiv.id = 'loading-div-for-schedule';
      loadingDiv.classList.add('is-hidden');
      document.body.appendChild(loadingDiv);
    }
    const parent = document.getElementById(`s-${dayId}`);
    const response = await fetch(`/cohorts/${cohortId}/days/${dayId}/schedule/`);
    if (response.ok) {
      const html = await response.text();
      loadingDiv.innerHTML = html;
      const replacement = loadingDiv.querySelector(`#s-${dayId}`);
      if (parent.classList.contains('is-hidden')) {
        replacement.classList.add('is-hidden');
      }
      parent.parentNode.replaceChild(replacement, parent);
    }
  }

  let [hash, subHash] = (window.location.hash || '#noop').substring(1).split('/');
  const tab = document.querySelector(`a[href="#${hash}"]`);
  if (tab) {
    tab.click();
    if (subHash) {
      const subTab = document.querySelector(`a[href="#${hash}/${subHash}"]`);
      if (subTab) {
        subTab.click();
      }
    }
  }

  let currentSchedule = document.querySelector('#schedule .schedule-table');
  if (currentSchedule) {
    setInterval(() => updateSchedule(), 1000 * 30);
    updateSchedule(currentSchedule);
  }

  function updateActivity(keyName, payload, callback) {
    if (callback === undefined && typeof payload === 'function') {
      callback = payload;
      payload = (_, value) => ({[keyName]: value});
    } else if (callback === undefined && payload === undefined) {
      callback = () => {};
      payload = (_, value) => ({[keyName]: value});
    }
    return async function _updateActivity(e) {
      const target = e.target;
      target.classList.remove('input-success');
      const value = parseInt(target.options[target.selectedIndex].value);
      const activity_id = target.dataset.activityId;
      const url = `/cohorts/activity/${activity_id}/update`;
      const fetchOptions = {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload(e, value)),
      };
      const response = await fetch(url, fetchOptions);
      if(response.ok) {
        target.classList.add('input-success');
        callback(e);
        showSuccessMessage('Successfully updated that activity');
      } else if (response.status == 403) {
        showErrorMessage('You do not have permission to change that');
        target.value = oldValue;
        console.log(oldValue, target);
      } else {
        showErrorMessage('Failed to update that activity (see console for error)');
        console.error(response.status);
        console.error(await response.text());
      }
    };
  }

  globalAddEventListener('change', 'instructor-assignment', updateActivity('instructor_id'));

  globalAddEventListener(
    "animationend",
    "instructor-assignment",
    async (e) => {
      const target = e.target;
      target.classList.remove('input-success');
    }
  )

  globalAddEventListener('change', 'pod-instructor-assignment', updateActivity(
    null,
    (e, value) => ({
      instructor_id: value,
      pod_id: Number.parseInt(e.target.dataset.podId),
    }),
    () => {},
  ));

  globalAddEventListener(
    "change",
    "feedback-assignment",
    async (e) => {
      const target = e.target;
      target.classList.remove('input-success');
      const value = parseInt(target.options[target.selectedIndex].value);
      const day_id = target.dataset.dayId;
      const url = `/cohorts/day/${day_id}/feedback/assign`;
      const fetchOptions = {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({instructor_id: value}),
      };
      const response = await fetch(url, fetchOptions);
      if(response.ok) {
        target.classList.add('input-success');
        showSuccessMessage('Successfully updated that day');
      } else {
        showErrorMessage('Failed to update that day (see console for error)');
        console.error(response.status);
        console.error(await response.text());
      }
    }
  );

  globalAddEventListener(
    "animationend",
    "feedback-assignment",
    async (e) => {
      const target = e.target;
      target.classList.remove('input-success');
    }
  )

  globalAddEventListener('change', 'location-assignment', updateActivity('location_id', e => {
    const target = e.target;
    const activityId = target.dataset.activityId;
    const value = Number.parseInt(target.options[target.selectedIndex].value);
    const leader = document.querySelector(`#leader-${activityId}`);
    const podLeaders = document.querySelector(`#pod-leader-${activityId}`);
    const breakoutRoom = document.querySelector(`#breakout-room-${activityId}`)
    if (value === 2) {
      leader.classList.add('is-hidden');
      podLeaders.classList.remove('is-hidden');
    } else {
      leader.classList.remove('is-hidden');
      podLeaders.classList.add('is-hidden');
      if (value === 3) {
        breakoutRoom.classList.remove('is-hidden');
      } else {
        breakoutRoom.classList.add('is-hidden');
      }
    }
    const schedule = document.getElementById(`s-${target.dataset.dayId}`);
    for (let dropdown of schedule.querySelectorAll('select')) {
      dropdown.disabled = true;
    }
    loadSchedule(target.dataset.cohortId, target.dataset.dayId);
  }));

  const dropdowns = document.querySelectorAll('.dropdown');
  if (dropdowns.length > 0) {
    document.addEventListener('click', () => {
      for (const dropdown of dropdowns) {
        dropdown.classList.remove('is-active');
      }
    });
  }
  for (const dropdown of dropdowns) {
    const trigger = dropdown.querySelector('.dropdown-trigger');
    trigger.addEventListener('click', event => {
      event.preventDefault();
      event.stopPropagation();
      dropdown.classList.toggle('is-active');
    });
  }

  const gradebook = document.querySelector('#gradebook-table');
  if (gradebook) {
    gradebook.addEventListener('change', e => {
      if (e.target.tagName != 'INPUT') {
        return;
      }

      const releaseButtons = document.querySelectorAll('.release-button');
      for (const button of releaseButtons) {
        button.disabled = true;
      }

      const downloadButton = document.querySelector('#download-button');
      console.log("download button", downloadButton);
      if (downloadButton) {
        downloadButton.tabIndex = -1;
        downloadButton.style.opacity = 0.5;
        downloadButton.style.cursor = 'not-allowed';
        downloadButton.addEventListener('click', e => e.preventDefault());
      }
    });
  }
});
