So you want to get the golden snitch huh?
or maybe you just want to not worry about grabbing the token for attendence.

what ever your reason is, this script will help. 

I don't believe in handouts, only hand-ups! 

So, with that being said my code does have quite a few landmines scattered throughout (by design).
This is so you can better identify HOW: this was written, as well as give you a working outline as to the process and/or challenges of building your own. 

So what is the overall goal of the script?

grab a dynamically dropped object, a 'string' in our case, pull that 'string' out of all the information on the page, paste that 'string' into an available field and submit. 

This may be a little daunting to look at and take in, INITIALLY. But, I promise after you kind of understand a few things in the code it all just kind of falls into line. 

So everything to the left of a '=' is correct, I tried to be as explicit as I could in my naming conventions to make it a little less convoluted. Anything to the RIGHT of a '=' is fair game as to changes, so this is where you should be maintaining your attention. Look at What is the left of '=' to verify what you are looking for and everything to the right is what you are doing, So if you are searching for a CSS_selector then it should have a proper format as to how that is being called, or the value of the 'string' may need more explicit naming as a couple of examples. 

All of the information that was used to write this is available at https://www.selenium.dev/ 

Good luck on grabbing your golden snitch
