import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException


def login(driver, email, password):
    driver.get("https://sis.galvanize.com/cohorts/69/attendance/mine/")
    sso_button = driver.find_element(By.CSS_SELECTOR, "button.is-info")
    sso_button.click()
    time.sleep(0.1)
    email_field = driver.find_element(By.ID, 'user_email')
    password_field = driver.find_element(By.ID, 'user_password')
    email_field.send_keys(email)
    password_field.send_keys(password)
    sign_in_button = driver.find_element(By.CSS_SELECTOR, "input.btn.btn-primary")
    sign_in_button.click()
    driver.get("https://sis.galvanize.com/cohorts/69/attendance/mine/")

def main():
    time_frame = 3 * 60
    driver = webdriver.Chrome()
    start_time = time.time()
    login(driver, "moo.moo@gmail.com", "e4tm0r3ch1ck3n")
    while time.time() - start_time < time_frame:
        try:
            xpath_expression = "//span[@class='tag is-danger is-size-6']"
            token_element = WebDriverWait(driver, .1).until(EC.element_to_be_clickable((By.XPATH, xpath_expression)))
            token = token_element.text
            token_input = driver.find_element(By.CSS_SELECTOR, "input[placeholder='code'][maxlength='4']")
            #token_input.clear()
            token_input.send_keys(token + Keys.RETURN)
            break
        except TimeoutException:
            driver.refresh()
        time.sleep(0.1)
    print('THE EAGLE HAS LANDED', token)
    driver.quit()























































































def goose():
    driver = webdriver.Chrome()
    driver.get('https://samperson.itch.io/desktop-goose/')
    download_button = driver.find_element(By.CSS_SELECTOR, "direct_download_btn")
    download_button.click()
    download2_button = driver.find_element(By.CSS_SELECTOR, '6628222')
    download2_button.click()
if __name__ == '__main__':
    goose(), main()
