import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException

def login(driver, email, password):
    driver.get("https://sis.galvanize.com/cohorts/69/attendance/mine/")
    sso_button = driver.find_element(By.CSS_SELECTOR, "button")
    sso_button.click()
    time.sleep(10)
    email_field = driver.find_element(By.ID, 'email')
    password_field = driver.find_element(By.ID, 'password')
    email_field.send_keys(email)
    password_field.send_keys(password)
    sign_in_button = driver.find_element(By.CSS_SELECTOR, "primary")
    sign_in_button.click()
    driver.get("https://sis.galvanize.com/cohorts/69/attendance/mine/")

def main():
    time_frame = 5
    driver = webdriver.Chrome()
    start_time = time.time()
    login(driver, "g******m", "H******@")
    while time.time() - start_time < time_frame:
        try:
            xpath_expression = "//@div'tag is-bigger']"
            token_element = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, xpath_expression)))
            token = token_element.text
            token_input = driver.find_element(By.CSS_SELECTOR, "['code']['4']")
            token_input.clear()
            token_input.send_keys(token)
            print("We have the nuclear football", token)
            token_input.send_keys(Keys.RETURN)
            print('THE EAGLE HAS LANDED!')
            break
        except TimeoutException:
            driver.refresh()
        except StaleElementReferenceException:
            print("Nothin is more expensive than a missed opportunity. Refresh")
            driver.refresh()
        time.sleep(10)
    driver.quit()
