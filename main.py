import time
from datetime import datetime
from selenium import webdriver
# from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
# from selenium.webdriver.support import expected_conditions as EC
from keys import EMAIL, PASSWORD
import sys

# this opens into a new browser!
# Import Cookies? too hard!
# make it log in for me
# Thanks Gage, I'll take it from here
# make this a main func
if __name__ == "__main__":

    while True:
        t = time.localtime(time.time())
        windows = [
            t.tm_hour == 7 and t.tm_min > 55,
            t.tm_hour == 12 and t.tm_min > 40,
            t.tm_hour == 15 and t.tm_min > 55,
        ]
        print(t)

        if (any(windows)):
            print("Window is open!")
            time_frame = 6 * 60
            driver = webdriver.Chrome()
            start_time = time.time()

            # log in first, then loop over the attendance page

            # webpage loads at Single Sign-On Portal - go to Login page
            driver.get("https://sis.galvanize.com/cohorts/69/attendance/mine/")
            sso_button = driver.find_element(by=By.CSS_SELECTOR, value="button")
            sso_button.click()

            # find email and password fields and enter my info.
            email_field = driver.find_element(by=By.ID, value="user_email")
            password_field = driver.find_element(by=By.ID, value='user_password')
            email_field.send_keys(EMAIL)
            password_field.send_keys(PASSWORD)

            # click "Sign In"
            sign_in_button = driver.find_element(by=By.NAME, value="commit")
            sign_in_button.click()

            # Load the Attendance page
            driver.get("https://sis.galvanize.com/cohorts/69/attendance/mine/")

            # loop until 5 minutes elapse
            i = 0
            while time.time() - start_time < time_frame:
                driver.refresh()
                token = None
                try:
                    token = driver.find_element(
                        by=By.CSS_SELECTOR, value=".is-danger").text
                    text_field = driver.find_element(By.NAME, 'token')
                    text_field.send_keys(token)
                    attendance_button = driver.find_element(
                        by=By.CSS_SELECTOR, value=".is-primary")
                    attendance_button.click()
                    print(datetime.now())
                    print('Got Token:', token)
                    break
                except NoSuchElementException:
                    print("No token. Refresh!")
                    print(i)
                    i += 1
                    time.sleep(0.001)

            time.sleep(60)
            driver.quit()
            time.sleep(6000)
        time.sleep(240)
